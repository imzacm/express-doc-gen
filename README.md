# express-doc-gen 

A module I made for my own use that gets all routes in an express app and creates documentation for them using a template and a json file to specify parameters and descriptions. This probably won&#39;t ever be updated.

## Installation

```sh
npm install express-doc-gen --save
```

## Use
```
const docGen = require('express-doc-gen');

var options = {
  template,// - optional (uses default template if not specified) -- path to custom template
  docInfo,// - optionish (populates with test one if not specified) -- path to docInfo.json
  docPath// - optional (uses __dirname/documentation if not specified) -- folder to store documentation in
};
docGen.generate(app, options)
  .then(function(routes) {
    console.log('Detected routes: ' + JSON.stringify(routes));
  });
```

## Tests

```sh
npm install
npm test
```
```

> express-doc-gen@1.0.0 test /var/www/html/ide/workspace/express-doc-gen
> mocha tests/*Test.js
  Generate docs
    ✓ should return an array with two routes
  1 passing (29ms)

```

## Dependencies

- [mkdirp](https://github.com/substack/node-mkdirp): Recursively mkdir, like `mkdir -p`
- [q](https://github.com/kriskowal/q): A library for promises (CommonJS/Promises/A,B,D)

## Dev Dependencies

- [chai](https://github.com/chaijs/chai): BDD/TDD assertion library for node.js and the browser. Test framework agnostic.
- [express](https://github.com/expressjs/express): Fast, unopinionated, minimalist web framework
- [mocha](https://github.com/mochajs/mocha): simple, flexible, fun test framework


## License

MIT
