const appRoot = require('app-root-path');
const packageJSON = require(appRoot + '/package.json');
const mkdirp = require('mkdirp');
const path = require('path');
const fs = require('fs');
const q = require('q');

module.exports = {
	/*
	generate (options)
	app - required -- express app (add 'module.exports = app' to the end of main file e.g. index.js) (Or use this in the app and just pass the app object)
	options - optional = {
		template, -- path to custom template
		docInfo, -- path to docInfo.json
		docPath -- folder to store documentation in
	}
	*/
	generate: function(app, options) {
		var deferred = q.defer();
		var templateFile = typeof options == 'object' && typeof options.template == 'string' ? options.template : path.resolve(__dirname) + '/docTemplate.html';
		var docInfoFile = typeof options == 'object' && typeof options.docInfo == 'string' ? options.docInfo : path.resolve(__dirname) + '/docInfo.json';
		var docPath = typeof options == 'object' && typeof options.docPath == 'string' ? options.docPath : appRoot + '/documentation';

		var startTime = new Date();
		mkdirp(path.join(docPath, '/' + packageJSON.name + packageJSON.version), function() {
			var routes = [];

			function print(path, layer) {
				if (layer.route) {
					layer.route.stack.forEach(print.bind(null, path.concat(split(layer.route.path))))
				} else if (layer.name === 'router' && layer.handle.stack) {
					layer.handle.stack.forEach(print.bind(null, path.concat(split(layer.regexp))))
				} else if (layer.method) {
					routes.push({
						method: layer.method.toUpperCase(),
						path: path.concat(split(layer.regexp)).filter(Boolean).join('/')
					});
					// console.log('%s /%s',
					//     layer.method.toUpperCase(),
					//     path.concat(split(layer.regexp)).filter(Boolean).join('/'))
				}
			}

			function split(thing) {
				if (typeof thing === 'string') {
					return thing.split('/')
				} else if (thing.fast_slash) {
					return ''
				} else {
					var match = thing.toString()
						.replace('\\/?', '')
						.replace('(?=\\/|$)', '$')
						.match(/^\/\^((?:\\[.*+?^${}()|[\]\\\/]|[^.*+?^${}()|[\]\\\/])*)\$\//)
					return match ? match[1].replace(/\\(.)/g, '$1').split('/') : '<complex:' + thing.toString() + '>'
				}
			}
			app._router.stack.forEach(print.bind(null, []));

			var list = [];
			var docParams = require(docInfoFile);
			routes.forEach(function(route) {
				var listItem = '<tr><td>' + route.method + '</td><td>' + route.path + '</td>';
				if (typeof docParams[route.path] == 'object' && typeof docParams[route.path].params == 'object') {
					listItem += '<td><ul class="list-group">';
					var params = docParams[route.path].params;
					for (key in params) {
						listItem += '<li class="list-group-item">';
						listItem += key + ' - ' + params[key];
						listItem += '</li>';
					}
					listItem += '</ul></td>';
				} else {
					listItem += '<td></td>';
				}
				if (typeof docParams[route.path] == 'object' && typeof docParams[route.path].description != 'undefined') {
					listItem += '<td>' + docParams[route.path].description + '</td>';
				} else {
					listItem += '<td></td>';
				}
				listItem += '</tr>\n';
				list.push(listItem);
			});

			var endTime = new Date();
			var genTime = endTime - startTime;

			var template = fs.readFileSync(templateFile, 'utf-8');
			template = template.replace('{{DOC_TABLE}}', list.join(''));
			template = template.replace('{{APP_NAME}}', packageJSON.name);
			template = template.replace('{{APP_VERSION}}', packageJSON.version);
			template = template.replace('{{DOC_GEN_DATE_TIME}}', startTime.toString());
			template = template.replace('{{DOC_GEN_TIME}}', genTime.toString());
			fs.writeFileSync(path.join(docPath, '/' + packageJSON.name + packageJSON.version) + '/' + packageJSON.name + '.html', template);
			deferred.resolve(routes);
		});
		return deferred.promise;
	}
};
