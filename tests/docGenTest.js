var assert = require('chai').assert;
var expect = require('chai').expect;
var should = require('chai').should();
var chai = require('chai');

var express = require('express');
var app = express();

app.get('/thing/doThing', function(req, res) {
});
app.post('/otherThing/doOtherThing', function(req, res){
});

var path = require('path');
var docGen = require('../doc.js');

describe('Generate docs', function() {
    it('should return an array with two routes', function(){
        return docGen.generate(app, {
            template: path.join('./') + 'docTemplate.html',
            docInfo:  path.join('./') + 'docInfo.json',
            docPath: path.join('./tests', 'documentation')
        }).then(function(routes) {
            expect(routes[0]).to.deep.equal({method: 'GET', path: 'thing/doThing'});
            expect(routes[1]).to.deep.equal({method: 'POST', path: 'otherThing/doOtherThing'});
        });
    });
});
